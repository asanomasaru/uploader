<?PHP include('assets/php/ini.php'); ?>
<!doctype html>
<html lang="ja">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="robots" content="noindex">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://labo.sarustar.net/cdn/destyle.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.0/font/bootstrap-icons.css">
	<link href="assets/css/style.css" rel="stylesheet">
	<title><?=$ini['title'];?>管理画面</title>
</head>
<body>
<header>
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="header_inner">
					<h1 class="site_title"><?=$ini['title'];?>管理画面</h1>
					<ul class="global_menu">
						<li class="global_config js_btn" onclick="on_config_toggle()">設定</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</header>
<main>
	
	<section class="image_list" onclick="on_stage_click()">
		<input type="text" name="album_title" placeholder="タイトルを入力してください">
		<ul class="image_list" data-aid="-1" data-created="" data-modified="" id="ul_image_list" data-cover="" data-tags="">
			<template id="album_image">
				<li class="album_image" onclick="on_image_click(this,event)">
					<div class="img handle" 
						  style="background-image:url(_doc/images/{{dir}}/{{filename}})" 
						  data-filename="{{filename}}" 
						  data-description="{{description}}"
						  data-tags="{{tags}}">
						<ul class="tags_label show"></ul>
					</div>
				</li>
			</template>
			<li class="add">
				<form method="post" enctype="multipart/form-data" id="add_image_form">
					<label>
						<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus-circle-fill" viewBox="0 0 16 16">
							<path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8.5 4.5a.5.5 0 0 0-1 0v3h-3a.5.5 0 0 0 0 1h3v3a.5.5 0 0 0 1 0v-3h3a.5.5 0 0 0 0-1h-3v-3z"/>
						</svg>
						<input type="file" name="uploadImages[]" multiple accept="image/*" onchange="on_add_images()">
					</label>
				</form>
			</li>
		</ul>
		<article class="album_text">
			<h1><?=$ini['title'];?>テキスト</h1>
			<textarea></textarea>
			<?PHP
			if(array_key_exists('custom_fields',$c)){
				if(count($c['custom_fields']) > 0){
			?>
			<h1>カスタムフィールド</h1>
			<?PHP	
				};
			?>
			<ul class="custom_fields">
				<?PHP foreach($c['custom_fields'] as $k => $cf){ ?>
					<li class="custom_field">
						<p><?=$cf['key'];?></p>
						<input 
								name="value" 
								class="<?=$cf['key'];?>" 
								type="<?=$cf['type'];?>" 
								placeholder="value" />
					</li>
				<?PHP };}; ?>
			</ul>
			<div class="article_closer js_btn" onclick="on_article_closer(this)">
				<svg width="100%" height="100%"><line x1="15%" y1="50%" x2="85%" y2="50%" stroke="currentColor" stroke-width="1px" /></svg>
			</div>
		</article>
		<input class="btn_save js_btn" onclick="on_save()" type="button" value="保存" />
	</section>
	
	<section class="editor">
		<div class="editor_inner">
			<article class="album_infomation">
				<h1><?=$ini['title'];?>情報</h1>
				<ul class="infomation">
					<li class="album_infomation_title"><span class="label">タイトル</span> / <span class="body"></span></li>
					<li class="album_infomation_created"><span class="label">作成日時</span> / <span class="body"></span></li>
					<li class="album_infomation_modified"><span class="label">保存日時</span> / <span class="body"></span></li>
					<li class="album_infomation_count"><span class="label">画像枚数</span> / <span class="body"></span></li>
					<li class="album_infomation_cover"><span class="label">カバー画像</span> / <br><div class="infomation_cover" style=""></div></li>
				</ul>
				<ul class="tools">
					<li>
						<input type="range" oninput="on_change_image_size()" min="1" max="20" step="1" class="input-range" id="image_size_range">
					</li>
				</ul>
				<p class="editor_btn js_btn btn_album_delete" onclick="on_album_delete()"><?=$ini['title'];?>削除</p>
				<div class="article_closer js_btn" onclick="on_article_closer(this)">
					<svg width="100%" height="100%"><line x1="15%" y1="50%" x2="85%" y2="50%" stroke="currentColor" stroke-width="1px" /></svg>
				</div>
			</article>
			<article class="editor">
				<p class="selected_count"><span class="selected_count">0</span>件が選択されています。</p>
				<p class="editor_btn js_btn btn_selected_delete" onclick="on_selected_delete()">削除</p>
				<p class="editor_btn js_btn btn_unselect" onclick="on_unselect()">選択解除</p>
				<p class="editor_btn js_btn btn_set_cover" onclick="on_set_cover()">選択画像をカバーに設定</p>
				<div class="article_closer js_btn" onclick="on_article_closer(this)">
					<svg width="100%" height="100%"><line x1="15%" y1="50%" x2="85%" y2="50%" stroke="currentColor" stroke-width="1px" /></svg>
				</div>
			</article>
			<?PHP if( $ini['tags_target'] == 'image' && array_key_exists('tags',$c)){ ?>
			<article class="tags">
				<div class="tags_inner">
					<h1><?=$ini['tags_title'];?></h1>
					<ul class="tags">
						<?PHP
						foreach($c['tags'] as $i => $v){
							if($v['status']){
						?>
						<li>
							<label>
								<input type="checkbox" id="input_tag_<?=$v['tid'];?>" value="<?=$v['tid'];?>" onchange="on_toggle_tag('image')">
								<?=$v['name'];?>
							</label>
						</li>
						<?PHP }}; ?>
					</ul>
				</div>
				<div class="article_closer js_btn" onclick="on_article_closer(this)">
					<svg width="100%" height="100%"><line x1="15%" y1="50%" x2="85%" y2="50%" stroke="currentColor" stroke-width="1px" /></svg>
				</div>
				<div class="tags_label_toggle js_btn data-tooltip" onclick="on_tags_label_toggle()" data-tooltip="データツールチップ">
					<svg width="100%" height="100%"><circle cx="50%" cy="50%" r="45%" fill="none" stroke="currentColor" stroke-width="1px" /></svg>
				</div>
			</article>
			<?PHP }; ?>
			
			<?PHP if( $ini['tags_target'] == 'album' && array_key_exists('tags', $c)){ ?>
			<article class="album_tags">
				<div class="tags_inner">
					<h1><?=$ini['title'];?><?=$ini['tags_title'];?></h1>
					<ul class="tags show">
						<?PHP
						foreach($c['tags'] as $i => $v){
							if($v['status']){
						?>
						<li>
							<label>
								<input type="checkbox" id="input_tag_<?=$v['tid'];?>" value="<?=$v['tid'];?>" onchange="on_toggle_album_tag('album')">
								<?=$v['name'];?>
							</label>
						</li>
						<?PHP }}; ?>
					</ul>
				</div>
				<div class="article_closer js_btn" onclick="on_article_closer(this)">
					<svg width="100%" height="100%"><line x1="15%" y1="50%" x2="85%" y2="50%" stroke="currentColor" stroke-width="1px" /></svg>
				</div>
			</article>
			<?PHP }; ?>
			<div class="handle js_btn">
				<p onclick="on_toggle_editor()"><?PHP include('assets/img/arrow_down.svg');?></p>
			</div>
			
		</div>
	</section>
	
	<section class="album_list album_list_style_<?=$ini['album_list_style'];?>">
		<h1><?=$ini['title'];?>一覧</h1>
		<div class="handle js_btn" onclick="on_toggle_album_list()">
			<p><?PHP include('assets/img/arrow_down.svg');?></p>
		</div>
		<div class="album_list_wrapper">
			<ul class="album_list sortable" id="ul_album_list">
				<li class="add" onclick="on_add_album()">
					<form method="post" enctype="multipart/form-data" id="add_album_form">
						<label>
							<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus-circle-fill" viewBox="0 0 16 16">
								<path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8.5 4.5a.5.5 0 0 0-1 0v3h-3a.5.5 0 0 0 0 1h3v3a.5.5 0 0 0 1 0v-3h3a.5.5 0 0 0 0-1h-3v-3z"/>
							</svg>
							<!--<input type="file" name="uploadImages[]" multiple accept="image/*" onchange="on_add_album()">-->
						</label>
					</form>
				</li>
				<template id="album_cover">
					<li
						 class="album_cover cover_{{aid}} handle"
						 onclick="on_select_album({{aid}})"
						 data-aid="{{aid}}">
						<div class="album_list_thumbnail" style="background-image:url(_doc/images/{{dir}}/{{cover}})"></div>
						<?PHP
						if($ini['album_list_style'] == 'list'){
						?>
						<div class="album_list_info">
							<p class="date">{{custom_fields--date}}</p>
							<h2 class="title">{{title}}</h2>
							<p class="description">{{description}}</p>
						</div>
						<?PHP
						};
						?>
					</li>
				</template>
			</ul>
		</div>
	</section>
	
	<section class="config">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<article class="config_tags">
						<h1><span class='tags_title'><?=$ini['tags_title']?></span>設定</h1>
						<ul class="config_tags">
							<?PHP 
								if(array_key_exists('tags',$c)){
									foreach($c['tags'] as $i => $tag_obj){
							?>
							<li class="tag" data-status="<?=$tag_obj['status'];?>" data-tid="<?=$tag_obj['tid'];?>">
								<p><span class="tid"><?=$tag_obj['tid'];?></span><input name="name" value="<?=$tag_obj['name'];?>"></p>
								<div class="btns">
									<div class="config_tag_delete js_btn" onclick="on_config_tag_delete(this)">
										<svg width="100%" height="100%">
											<line x1="0" y1="0" x2="100%" y2="100%" stroke="currentColor" stroke-width="1px" />
											<line x1="0" y1="100%" x2="100%" y2="0" stroke="currentColor" stroke-width="1px" />
										</svg>
									</div>
								</div>
							</li>
							<?PHP }}; ?>
							<li class="add_tag_btn js_btn" onclick="on_config_tag_add()"><?=$ini['tags_title']?>追加</li>
							<template id="config_tag_template">
								<li class="tag" data-status="1" data-status="1" data-tid="{{tid}}">
									<p><span class="tid">{{tid}}</span><input name="name" value=""></p>
									<div class="btns">
										<div class="config_tag_delete js_btn" onclick="on_config_tag_delete(this)">
											<svg width="100%" height="100%">
												<line x1="0" y1="0" x2="100%" y2="100%" stroke="currentColor" stroke-width="1px" />
												<line x1="0" y1="100%" x2="100%" y2="0" stroke="currentColor" stroke-width="1px" />
											</svg>
										</div>
									</div>
								</li>
							</template>
						</ul>
					</article>
				</div>
				<div class="col-lg-6">
					<article class="config_custom_field">
						<h1>カスタムフィールド設定</h1>
						<ul class="config_custom_fields">
							<?PHP
								if(array_key_exists('custom_fields',$c)){
									foreach($c['custom_fields'] as $key => $cf){
							?>
							<li class="config_custom_field">
								<input type="text" placeholder="key" value="<?=$cf['key'];?>">
								<select style="display: none;">
									<option value="<?=$cf['type'];?>" selected><?=$cf['type'];?></option>
								</select>
								<div class="btns">
									<div class="config_custom_field_delete_btn js_btn" onclick="on_config_custom_field_delete(this)">
										<svg width="100%" height="100%">
											<line x1="0" y1="0" x2="100%" y2="100%" stroke="currentColor" stroke-width="1px" />
											<line x1="0" y1="100%" x2="100%" y2="0" stroke="currentColor" stroke-width="1px" />
										</svg>
									</div>
								</div>
							</li>
							<?PHP }}; ?>
							<template id="template_config_custom_field">
								<li class="config_custom_field">
									<input type="text" placeholder="key" value="{{key}}">
									<select>
										<option value="text">text</option>
										<option value="date">date</option>
										<option value="color">color</option>
									</select>
									<div class="btns">
										<div class="config_custom_field_delete_btn js_btn" onclick="on_config_custom_field_delete(this)">
											<svg width="100%" height="100%">
												<line x1="0" y1="0" x2="100%" y2="100%" stroke="currentColor" stroke-width="1px" />
												<line x1="0" y1="100%" x2="100%" y2="0" stroke="currentColor" stroke-width="1px" />
											</svg>
										</div>
									</div>
								</li>
							</template>
						</ul>
						<p class="js_btn" onclick="on_add_custom_field()">カスタムフィールド追加</p>
					</article>
				</div>
				<div class="col-12">
					<input class="btn_save js_btn" onclick="on_config_save()" type="button" value="保存" />
				</div>
			</div>
	</section>
		
</main>
<div id="howtouse" class="" onclick="on_htu_close()">
	<div class="htu_container">
		<div id="htu_0"><img src="assets/img/htu_0.png"></div>
	</div>
</div>
<div id="nowloading">
	<img src="assets/img/nowloading.svg">
</div>
<footer>
</footer>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="assets/js/Sortable.min.js" type="text/javascript"></script>
<script src="assets/js/uploader.js" type="text/javascript"></script>
<script src="assets/js/data-tooltip.js" type="text/javascript"></script>
</body>
</html>