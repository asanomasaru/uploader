<?PHP

$tid = $_GET['tid'];

$a = file_get_contents('../../../../albums.json');
$a = json_decode($a,true);
$res = array();

$tagged_images = array();
foreach($a as $aid => $album){
	$images = $album['images'];
	foreach($images as $image){
		$tags = explode(',',$image['tags']);
		foreach($tags as $tag){
			if($tid == $tag){
				array_push($tagged_images,$image);
			};
		};
	};
};

$json = json_encode($tagged_images);
echo $json;

?>