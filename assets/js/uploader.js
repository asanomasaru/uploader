var albums = {};
var config = {};
var wW, wH;
var album_scroll_count = 0;
//var album;

$(function () {
	console.log('uploader.js');

	$.getJSON('_doc/config.json?v=' + now('str'))
		.done(function (json) {
			config = json;
			load_album();
		})
		.fail(function (jqXHR, textStatus, errorThrown) {
			$('#howtouse').addClass('show');
		})
		.always(function () {});

	function load_album() {
		$.getJSON('_doc/albums.json?v=' + now('str'))
			.done(function (json) {
				//console.log('done');
				albums = json;
				setWWH();
				set_image_size_range();
				set_album_list();
				set_album_list_WWH();
				change_image_size();
			})
			.fail(function (jqXHR, textStatus, errorThrown) {
				$('#howtouse').addClass('show');
			})
			.always(function () {});
	};

	$(window).on('resize', function () {
		setWWH();
		set_album_list_WWH();
		set_image_size_range();
		change_image_size();
	});
});


/* ----------------------------------- btn func ----------------------------------- */

/*------------------------------------------ image_list ------------------------------------------*/
function on_add_images() {
	upload_images($('#add_image_form'), $('ul.image_list').attr('data-aid'));
};

function on_image_click(li, e) {
	//console.log(li);
	if ($(li).hasClass('selected')) {
		$(li).removeClass('selected');
	} else {
		$(li).addClass('selected');
	}
	e.stopPropagation();
	start_edit();
	tags_data_to_checkbox();
};

function on_stage_click() {
	$('li.album_image.selected').removeClass('selected');
	reset_input_tags();
	start_edit();
};
/*------------------------------------------ editor ------------------------------------------*/
function on_album_delete() {
	if (confirm('削除して良いですか？')) {
		var _aid = $('ul.image_list').attr('data-aid');
		delete albums['a_' + _aid];
		set_album_list();
		select_album(albums[Object.keys(albums)[0]]['aid']);
		on_save();
	}
};

function on_toggle_editor() {
	var _al = $('section.editor,section.image_list');
	if (_al.hasClass('closed')) {
		_al.removeClass('closed');
	} else {
		_al.addClass('closed');
	};
	set_album_list_WWH();
};

function on_set_cover() {
	//console.log($('li.album_image.selected div.img').attr('data-filename'));
	var filename = $('li.album_image.selected div.img').attr('data-filename');
	$('ul.image_list').attr('data-cover', filename);
	var aid = $('ul.image_list').attr('data-aid');
	$('.cover_' + aid + ' div.album_list_thumbnail').css('background-image', 'url(_doc/images/' + aid + '/' + filename + ')')
	refresh_album_infomation();
};

function on_selected_delete() {
	$('li.album_image.selected').remove();
	refresh_album_infomation();
};

function on_unselect() {
	on_stage_click();
};

function on_toggle_add_tag() {
	if ($('p.input_tag').hasClass('show')) {
		$('p.input_tag').removeClass('show')
	} else {
		$('p.input_tag').addClass('show')
	};
};

function on_toggle_tag(type) {
	checkbox_to_tags_data(type);
};

function on_toggle_album_tag(type) {
	checkbox_to_tags_data(type);
};

function on_article_closer(btn) {
	var article = $(btn).parents('article');
	if (article.hasClass('closed')) {
		article.removeClass('closed');
	} else {
		article.addClass('closed');
	};
};

function on_tags_label_toggle() {
	console.log('tags_label_toggle');
	var _tl = $('ul.image_list div.img ul.tags_label');
	if (_tl.hasClass('show')) {
		_tl.removeClass('show');
	} else {
		_tl.addClass('show');
	};
};

/*------------------------------------------ album_list ------------------------------------------*/

function on_add_album() {
	console.log('on_add_album');
	//clear_album();
	//upload_images($('#add_album_form'),'new');
	create_empty_album();
};

function on_toggle_album_list() {
	var _al = $('section.album_list');
	if (_al.hasClass('closed')) {
		_al.removeClass('closed');
	} else {
		_al.addClass('closed');
	};
};

function on_album_list_scroll(vec) {
	album_scroll_count += vec;
	$('ul.album_list').stop().animate({
		left: album_scroll_count * 180 + 'px'
	}, 300, 'easeOutCubic', function () {
		set_album_list_WWH();
	});
};

function on_select_album(id) {
	on_toggle_album_list();
	select_album(id);
};

/*------------------------------------------ config ------------------------------------------*/

function on_config_tag_add() {
	console.log('hoge');
	var _tid = {
		"tid": ($('.config_tags li.tag').length) + 1
	};
	$('article.config_tags li.add_tag_btn').before(template_to_html('config_tag_template', _tid));
};

function on_config_tag_delete(btn) {
	if (confirm('削除しても良いですか？')) {
		$(btn).parents('li').attr('data-status', 0);
	};
};

function on_add_custom_field() {
	console.log('on_add_custom_field');
	$('article.config_custom_field ul.config_custom_fields').append(template_to_html('template_config_custom_field', {
		"key": ''
	}));
};

function on_config_custom_field_delete(btn) {
	if (confirm('削除しても良いですか？')) {
		$(btn).parents('li').remove();
	};
};

function on_config_save() {
	console.log('on_config_save');
	nowloading();
	var tags = {};
	$.each($('ul.config_tags li.tag'), function (i, li) {
		var _tid = $(li).attr('data-tid');
		var _obj = {
			'tid': _tid,
			'name': $(li).find('input[name="name"]').val(),
			'status': $(li).attr('data-status')
		}
		tags['t_' + _tid] = _obj;
	});
	var custom_fields = {};
	$.each($('ul.config_custom_fields li.config_custom_field'), function (i, li) {
		var _key = $(li).find('input').val();
		var _obj = {
			'key': _key,
			'type': $(li).find('select').val()
		};
		custom_fields[_key] = _obj;
	});
	var _sd = {
		"tags": tags,
		"custom_fields": custom_fields
	};
	//console.log(_sd);
	$.ajax({
			url: 'assets/php/save_config.php',
			type: 'POST',
			dataType: 'html',
			data: _sd
		})
		.done(function (data) {
			console.log(data);
			loadingend();
			location.reload();
		})
		.fail(function (jqXHR, textStatus, errorThrown) {
			console.log('error : ' + jqXHR.status, textStatus);
		});
};

/*------------------------------------------ common ------------------------------------------*/

function on_save() {
	//console.log('on_save');
	nowloading();
	var obj = savedata_from_images();
	albums['a_' + obj['aid']] = obj;
	//console.log($('li.album_cover').length);
	//console.log(albums);
	var _custom_field_data = function () {
		var _cfd = {};
		$.each($('article.album_text li.custom_field'), function (i, li) {
			var _k = $(li).find('p').text();
			var _v = $(li).find('input[name="value"]').val();
			_cfd[_k] = _v;
		});
		//console.log(_cfd);
		return _cfd;
	};
	var _text = {
		'text': $('article.album_text textarea').val(),
		'custom_fields': _custom_field_data()
	};
	var _aid = $('ul.image_list').attr('data-aid');
	var _sd = {
		'text': _text,
		'albums': albums,
		'current_aid': _aid
	};
	//console.log(albums);
	$.ajax({
			url: 'assets/php/save_album.php',
			type: 'POST',
			dataType: 'html',
			data: _sd
		})
		.done(function (data) {
			loadingend();
			refresh_album_infomation();
		})
		.fail(function (jqXHR, textStatus, errorThrown) {
			console.log('error : ' + jqXHR.status, textStatus);
		});
};

function on_change_image_size() {
	//console.log('on_change_image_size');
	change_image_size();
};

function on_htu_close() {
	console.log('on_htu_close');
	$('#howtouse').removeClass('show');
};

function on_config_toggle() {
	console.log('config_toggle');
	if ($('section.config').hasClass('show')) {
		$('section.config').removeClass('show');
	} else {
		$('section.config').addClass('show')
	};
};

/* ----------------------------------- main func ----------------------------------- */

function create_empty_album() {
	nowloading();
	var _aid = now('str');
	var empty_albums = {};
	empty_albums['a_' + _aid] = {
		aid: _aid,
		dir: _aid,
		cover: 'noimage.png',
		created: now('dt'),
		modified: now('dt'),
		idx: '-1',
		images: [],
		status: 'active',
		title: ''
	};
	var _sd = {
		'aid': _aid,
		'albums': {
			...empty_albums,
			...albums
		}
	};

	$.ajax({
			url: 'assets/php/create_album.php',
			type: 'POST',
			dataType: 'json',
			data: _sd
		})
		.done(function (data) {
			console.log(data);
			location.reload();
			loadingend();
		})
		.fail(function (jqXHR, textStatus, errorThrown) {
			console.log('error : ' + jqXHR.status, textStatus, errorThrown);
			loadingend();
		});
};

function upload_images(form, aid) {
	nowloading();
	var _form = form;
	var _fdo = new FormData(_form[0]);
	var _images = [];
	var _aid;
	if (aid == 'new') {
		_aid = now('str');
	} else {
		_aid = aid;
	};
	_fdo.append('aid', _aid);
	$.ajax({
			url: 'assets/php/upload_images.php',
			//url: 'assets/php/upload_test.php',
			type: 'post',
			processData: false,
			contentType: false,
			data: _fdo,
			dataType: 'json'
		})
		.done(function (data) {
			//console.log(data);
			if (aid == 'new') {
				_on_new(data);
			} else {
				_on_add(data);
			};
		})
		.fail(function (jqXHR, textStatus, errorThrown) {
			console.log('error : ' + jqXHR.status, textStatus, errorThrown);
		});

	/*
	function _on_new(data){
		$.each(data['filenames'],function(i,v){
			var _image = {
				filename: v,
				description: '',
				tags: '',
				aid: _aid,
				dir: data['dir']
			};
			_images.push(_image);
		});
		var album = {
			"aid": _aid,
			"idx": -1,
			"title": '',
			"images": _images,
			"cover": _images[0]['filename'],
			"status": 'active',
			"created": now('dt'),
			"modified": now('dt'),
			"dir": data['dir']
		};
		append_images_from(album);
		on_save();
		set_album_list();
		set_album_list_WWH();
		loadingend();
	};
	*/

	function _on_add(data) {
		$.each(data['filenames'], function (i, v) {
			var _image = {
				filename: v,
				description: '',
				dir: data['dir']
			};
			_images.push(_image);
		});
		var album = albums['a_' + _aid];
		clear_album();
		if (Array.isArray(album['images'])) {
			album.images = $.merge(album.images, _images);
		} else {
			album.images = _images;
		}
		album.modified = now('dt');
		albums['a_' + _aid] = album;
		append_images_from(album);
		loadingend();
	};
};

function start_edit() {
	//console.log('start_edit');
	var selected_count = $('li.album_image.selected').length;
	$('section.editor p.selected_count span.selected_count').text(selected_count);
	//console.log(selected_count);
	if (selected_count == 0) {
		$('article.tags ul.tags').removeClass('show');
	} else {
		$('article.tags ul.tags').addClass('show');
	};
	$('.editor_btn').removeClass('show');
	if (selected_count == 1) {
		$('.btn_set_cover').addClass('show');
	};
	if (selected_count > 0) {
		$('.btn_selected_delete').addClass('show');
		$('.btn_unselect').addClass('show');
	};
};

function tags_data_to_checkbox() {
	var selected = $('ul.image_list li.album_image.selected div.img');
	//console.log(selected.length);
	var tags = [];
	$.each(selected, function (i, li) {
		var _arr = $(li).attr('data-tags').split(',');
		if (_arr.length > 0) {
			$.each(_arr, function (k, t) {
				tags.push(t);
			});
		};
	});
	if (selected.length == 0) {
		reset_input_tags();
	};
	if (selected.length > 1) {};
	//console.log(tags);
	$('section.editor article.tags ul.tags li input').prop('checked', false);
	$.each(tags, function (i, t) {
		$('section.editor article.tags ul.tags li #input_tag_' + t).prop('checked', true);
	});
};

function album_tags_data_to_checkbox() {
	//console.log('album_tags_data_to_checkbox');
	if ($('ul.image_list').attr('data-tags')) {
		var tags = $('ul.image_list').attr('data-tags').split(',');
		$.each(tags, function (i, t) {
			$('section.editor article.album_tags ul.tags li #input_tag_' + t).prop('checked', true);
		});
	};
};

function checkbox_to_tags_data(type) {
	if (type == 'image') {
		var target = $('section.editor article.tags ul.tags li input');
		var selected = $('ul.image_list li.album_image.selected div.img');
		var tags = [];
		$.each(target, function (i, c) {
			if ($(c).prop('checked')) {
				tags.push($(c).val());
			};
		});
		if (tags.length > 0) {
			tags = tags.join(',');
		} else {
			tags = '';
		};
		selected.attr('data-tags', tags);
		set_tags_label();
	} else if (type == 'album') {
		var target = $('section.editor article.album_tags ul.tags li input');
		var tags = [];
		$.each(target, function (i, c) {
			if ($(c).prop('checked')) {
				tags.push($(c).val());
			};
		});
		if (tags.length > 0) {
			tags = tags.join(',');
		} else {
			tags = '';
		};
		$('section.image_list ul.image_list').attr('data-tags', tags);
	};
};

function set_album_list() {
	//console.log('set_album_list');
	$('ul.album_list li.album_cover').remove();
	/*
	var pairs = Object.entries(albums);
	pairs.sort(function(p1, p2){
		var p1Key = p1[0], p2Key = p2[0];
		if(p1Key < p2Key){ return 1; }
		if(p1Key > p2Key){ return -1; }
		return 0;
	});
	albums = Object.fromEntries(pairs);
	*/
	//console.log(albums);
	$.each(albums, function (i, album) {
		//console.log(album);
		$('ul.album_list template#album_cover').before(template_to_html('album_cover', album));
	});
	if ($('ul.image_list').attr('data-aid') == -1) {
		select_album(albums[Object.keys(albums)[0]]['aid']);
	};
	console.log('album sortable');
	var el = document.getElementById('ul_album_list');
	set_album_sortable(el);
};

function select_album(id) {
	//console.log(albums['a_'+id]);
	clear_album();
	append_images_from(albums['a_' + id]);
	var _text = 'sample text';
	$.ajax({
			url: '_doc/texts/' + $('ul.image_list').attr('data-aid') + '.json',
			type: 'POST',
			dataType: 'json'
		})
		.done(function (data) {
			_text = data['text'];
			$.each(data['custom_fields'], function (k, v) {
				$('article.album_text ul.custom_fields li.custom_field input.' + k).val(v);
			});
			//$('article.album_text textarea').val(_text['text']);
			$('article.album_text textarea').val(_text);
			album_tags_data_to_checkbox();
		})
		.fail(function (jqXHR, textStatus, errorThrown) {
			$('article.album_text textarea').val('');
			//console.log('error : '+jqXHR.status,textStatus);
		});
};

function append_images_from(_album) {
	//console.log('append_images_from');
	$('ul.image_list').attr('data-aid', _album['aid']);
	$('ul.image_list').attr('data-created', _album['created']);
	$('ul.image_list').attr('data-modified', _album['modified']);
	$('ul.image_list').attr('data-cover', _album['cover']);
	$('ul.image_list').attr('data-dir', _album['dir']);
	$('ul.image_list').attr('data-tags', _album['tags']);
	$('section.image_list input[name="album_title"]').val(_album['title']);
	//$('article.album_text textarea').val(_album['text']);

	var src = '';
	$.each(_album['images'], function (i, img_obj) {
		//console.log(img_obj);
		src += template_to_html('album_image', img_obj);
	});
	$('ul.image_list li.add').before(src);

	//自動カバー設定
	if (Array.isArray(_album['images'])) {
		if (_album['images'].length > 0 && _album['cover'] == 'noimage.png') {
			//console.log('select cover');
			$('ul.image_list li').eq(0).addClass('selected');
			on_set_cover();
			$('ul.image_list li.selected').removeClass('selected');
		} else {
			//console.log('setted');
		};
	} else {
		//console.log('noimage');
	};

	refresh_album_infomation();
	var el = document.getElementById('ul_image_list');
	set_image_sortable(el);
	change_image_size();
	set_tags_label();
};

function savedata_from_images() {
	var _images = [];
	$.each($('.album_image div.img'), function (i, li) {
		var _image = {
			"filename": $(li).attr('data-filename'),
			"description": $(li).attr('data-description'),
			"tags": $(li).attr('data-tags'),
			"aid": $('ul.image_list').attr('data-aid'),
			"dir": $('ul.image_list').attr('data-dir')
		};
		_images.push(_image);
	});
	$('ul.image_list').attr('data-modified', now('dt'));
	var _custom_fields = {};
	$.each($('section.image_list article.album_text ul.custom_fields li.custom_field'), function (i, li) {
		_custom_fields[$(li).find('p').text()] = $(li).find('input').val();
	});
	//console.log('savedata : '+$('article.album_text textarea').val());
	var savedata = {
		"aid": $('ul.image_list').attr('data-aid'),
		"dir": $('ul.image_list').attr('data-dir'),
		"idx": -1,
		"title": $('section.image_list input[name="album_title"]').val(),
		"images": _images,
		"cover": $('ul.image_list').attr('data-cover'),
		"status": "active",
		"created": $('ul.image_list').attr('data-created'),
		"modified": $('ul.image_list').attr('data-modified'),
		"description": $('article.album_text textarea').val().substr(0, 50) + '...',
		"custom_fields": _custom_fields,
		"tags": $('ul.image_list').attr('data-tags')
	};
	return savedata;
};

/* ----------------------------------- support ----------------------------------- */
function set_tags_label() {
	//console.log('set_tags_label');
	$.each($('li.album_image div.img'), function (i, img) {
		//console.log($(img).attr('data-tags'));
		if ($(img).attr('data-tags')) {
			$(img).find('ul.tags_label').empty();
			var _tags_arr = $(img).attr('data-tags').split(',');
			$.each(_tags_arr, function (i, tid) {
				//console.log('i,tid : '+i+' / '+tid);
				if (tid !== 'undefined') {
					$(img).find('ul.tags_label').append('<li>' + config['tags']['t_' + tid]['name'] + '</li>');
				};
			});
		};
	});
};

function reset_input_tags() {
	$('section.editor article.tags ul.tags li input').prop('checked', false);
};

function refresh_album_infomation() {
	$('section.editor .album_infomation_title span.body').text($('section.image_list input[name="album_title"]').val());
	$('section.editor .album_infomation_created span.body').text($('ul.image_list').attr('data-created'));
	$('section.editor .album_infomation_modified span.body').text($('ul.image_list').attr('data-modified'));
	$('section.editor .album_infomation_count span.body').text($('li.album_image').length);
	$('section.editor .album_infomation_cover div.infomation_cover').css('background-image', 'url(_doc/images/' + $('ul.image_list').attr('data-dir') + '/' + $('ul.image_list').attr('data-cover') + ')');
}

function clear_album() {
	//アルバムの画像を全部消す
	$('.album_image').remove();
	$('section.image_list article.album_text ul.custom_fields li.custom_field input').val('');
};

function set_image_sortable(target) {
	var sortable = Sortable.create(target, {
		handle: ".handle",
		onUnchoose: function () {
			//console.log('set_image_sortable');
		}
	});
};

function set_album_sortable(target) {
	var sortable = Sortable.create(target, {
		handle: ".handle",
		onUnchoose: function () {
			var sorted_arr = {};
			$.each($('ul.album_list li.album_cover'), function (i, li) {
				var v = albums['a_' + $(li).attr('data-aid')];
				sorted_arr['a_' + $(li).attr('data-aid')] = v;
			});
			albums = sorted_arr;
			//console.log(albums);
		}
	});
};

function change_image_size() {
	//console.log('change_image_size : ');
	var max = parseInt($('#image_size_range').attr('max'));
	var count = parseInt((max + 1)) - parseInt($('#image_size_range').val());
	var listW = $('section.image_list').width();
	var image_size = (listW) / count;
	$('li.album_image, ul.image_list li.add').css({
		width: image_size + 'px',
		height: image_size + 'px'
	});
};

function template_to_html(template_id, obj) {
	var _obj = {};
	$.each(obj,function(k,v){
		if(typeof v === 'object'){
			$.each(v,function(kk,vv){
				_obj[k+'--'+kk] = vv;
			});
		} else {
			_obj[k] = v;
		}
	});
	obj = _obj;
	console.log(obj);
	//文字列としてテンプレートのinnerHTMLを取得
	var str = $('#' + template_id)[0].innerHTML;
	//{{}}で囲まれた文字列を検索するための正規表現
	var reg = /{{[a-zA-Z1-9_\-]*}}/g;
	var arr, replaced = str;
	while ((arr = reg.exec(str)) !== null) {
		//{{}}で囲まれた文字列をobjのキーとして置換
		var key = arr[0].replace('{{', '').replace('}}', '');
		replaced = replaced.replace(arr[0], obj[key]);
	};
	return replaced;
};

function setWWH() {
	wW = $(window).width();
	wH = $(window).height();
	var device = 'xs';
	if (wW < 576) {
		device = 'xs';
	} else if (wW <= 768) {
		device = 'sm';
	} else if (wW <= 992) {
		device = 'md';
	} else if (wW <= 1200) {
		device = 'lg';
	} else if (wW <= 1400) {
		device = 'xl';
	} else {
		device = 'xxl';
	};
	$('body').removeClass('xs sm md lg xl xxl').addClass(device);
	//console.log('wW : '+wW,'wH : '+wH);
};

function set_album_list_WWH() {
	/*
	var album_list_w = (obj_len(albums) + 1) * (60 + 10);
	//console.log(album_list_w);
	$('ul.album_list').css({'width':album_list_w+'px'});
	if($('ul.album_list').offset().left < 0){
		$('.scroller-right').addClass('hide');
	};
	$('.scroller_btn').removeClass('hide');
	if($('ul.album_list').offset().left + $('ul.album_list').width() < wW){
		$('.scroller-left').addClass('hide');
	};
	if($('ul.album_list').offset().left >= 10){
		$('.scroller-right').addClass('hide');
	};
	$('ul.image_list').css('min-height',(wH-$('ul.image_list').offset().top) + 'px');
	console.log($('ul.image_list').css('min-height'));
	*/
};

function set_image_size_range() {
	var min = 0;
	var max = 0;
	if ($('body').hasClass('xs')) {
		min = 1;
		max = 4;
	} else if ($('body').hasClass('sm')) {
		min = 1;
		max = 4;
	} else if ($('body').hasClass('md')) {
		min = 2;
		max = 5;
	} else if ($('body').hasClass('lg')) {
		min = 3;
		max = 10;
	} else if ($('body').hasClass('xl')) {
		min = 4;
		max = 12;
	} else if ($('body').hasClass('xxl')) {
		min = 4;
		max = 15;
	};
	$('#image_size_range').attr('min', min);
	$('#image_size_range').attr('max', max);
	$('#image_size_range').val(min);
};

function nowloading() {
	$('#nowloading').addClass('show');
};

function loadingend() {
	$('#nowloading').removeClass('show');
};
var now = function (mode) {
	var _now = new Date();
	var Y = String(_now.getFullYear()).padStart(2, '0');
	var m = String(_now.getMonth() + 1).padStart(2, '0');
	var d = String(_now.getDate()).padStart(2, '0');
	var H = String(_now.getHours()).padStart(2, '0');
	var i = String(_now.getMinutes()).padStart(2, '0');
	var s = String(_now.getSeconds()).padStart(2, '0');
	if (mode == 'str') {
		return Y + m + d + H + i + s;
	};
	if (mode == 'dt') {
		return Y + '-' + m + '-' + d + ' ' + H + ':' + i + ':' + s;
	};
};
var obj_len = function (_obj) {
	return _obj ? Object.keys(_obj).length : 1;
};
