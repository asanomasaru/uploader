<?PHP
//ini_set('display_errors', 0);
date_default_timezone_set('Asia/Tokyo');

$a; //albums
$c; //config
$ini; //ini

if(!file_exists('_doc/')){ mkdir('_doc'); };

$i_dir = '_doc/images/';
$t_dir = '_doc/texts/';
$c_url = '_doc/config.json';
$a_url = '_doc/albums.json';
$ini_url = 'ini.json';

if(!file_exists($i_dir)){ mkdir($i_dir); };

if(!file_exists($t_dir)){ mkdir($t_dir); };

if(file_exists($c_url)){
	$c = file_get_contents('_doc/config.json',true);
	$c = json_decode($c,true);
} else {
	$c = array('title' => 'Uploader', 'tags_title'=>'tags', 'tags'=>[]);
	$c_json = json_encode($c);
	file_put_contents($c_url,$c_json);
};

if(file_exists($a_url)){
	$a = file_get_contents($a_url,true);
	$a = json_decode($a,true);
} else {
	$a = array();
};

$ini = file_get_contents($ini_url,true);
$ini = json_decode($ini,true);

?>