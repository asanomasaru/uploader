<?PHP
date_default_timezone_set('Asia/Tokyo');
$aid = $_POST['aid'];
$files = $_FILES["uploadImages"]["tmp_name"];

$count = count($files);
$res = array(
	"length" => $count,
	"errors" => array(),
	"filenames" => array(),
	"datetime" => date("Y-m-d H:i:s"),
	"dir" => $aid
);
foreach($files as $i => $file_tmp){
	$extension = '';
	$imginfo = getimagesize($file_tmp);
	$img_name = date("YmdHis")."_".$i;

	if($imginfo['mime'] == 'image/jpeg'){ $extension = ".jpg"; }
	if($imginfo['mime'] == 'image/png'){ $extension = ".png"; }
	if($imginfo['mime'] == 'image/gif'){ $extension = ".gif"; }

	if(!empty($extension)){
		
		if(!file_exists("../../_doc/images/$aid")){
			mkdir("../../_doc/images/$aid");
		};
		
		$file_name = $img_name . $extension; // アップロード時のファイル名を設定
		$file_save = "../../_doc/images/$aid/" . $file_name; // アップロード対象のディレクトリを指定
		if(move_uploaded_file($file_tmp, $file_save)){
			array_push($res['filenames'],$file_name);
		} else {
			array_push($res['errors'],$i.' : '.$file_tmp.' : '.$file_save);
		}
	} else {
		array_push($res['filenames'],'not image');
		array_push($res['errors'],$i);
	};
};
echo json_encode($res);
?>